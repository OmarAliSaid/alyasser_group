package com.group.alyasser.database.model;

import android.content.Context;

import com.group.alyasser.database.MyDBHandler;

/**
 * Created by OmarAli on 27/01/2016.
 */
public class User{

    private int _id;
    private String _name,_password,_mail;
    private String _phone;
    private MyDBHandler dbHandler;
    public User(){
        this._name=null;
        this._password=null;
        this._mail=null;
        this._phone=null;
    };

    public User(String name,String password,String mail,String phone){
        this._mail=mail;
        this._name=name;
        this._password=password;
        this._phone=phone;
    }

    public User(String name,String password){
        this._name=name;
        this._password=password;
    }

    public String getName() {
        return _name;
    }

    public String getPassword() {
        return _password;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public void set_mail(String _mail) {
        this._mail = _mail;
    }

    public void set_phone(String _phone) {
        this._phone = _phone;
    }

    public String getMail() {
        return _mail;
    }

    public String getPhone() {
        return _phone;
    }

    public void Insert(Context context){
        dbHandler=new MyDBHandler(context,null,null,1);
        dbHandler.addUser(this);
    }

    public User getData(Context context){
        dbHandler=new MyDBHandler(context,null,null,1);
        return dbHandler.selectUser(this._mail);
    }

    public Boolean IsHaveAccess(Context context){
        dbHandler=new MyDBHandler(context,null,null,1);
        return dbHandler.checkAccess(this);
    }
}
