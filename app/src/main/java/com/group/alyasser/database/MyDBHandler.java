package com.group.alyasser.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.group.alyasser.database.model.User;

/**
 * Created by OmarAli on 27/01/2016.
 */
public class MyDBHandler extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="alyasser.db";
    public static final String TABLE_NAME="users";
    public static final String COLUMN_ID="_id";
    public static final String COLUMN_USERNAME="name";
    public static final String COLUMN_PASSWORD="password";
    public static final String COLUMN_EMAIL="mail";
    public static final String COLUMN_PHONE="phone";

    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query="CREATE TABLE "+TABLE_NAME+"( "
                +COLUMN_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
                +COLUMN_USERNAME+" VARCHAR(50) ,"
                +COLUMN_PASSWORD+" VARCHAR(50) ,"
                +COLUMN_EMAIL+" VARCHAR(50) ,"
                +COLUMN_PHONE+" VARCHAR(50)"+")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //add new user
    public void addUser(User user){

        ContentValues values=new ContentValues();
        values.put(COLUMN_USERNAME,user.getName());
        values.put(COLUMN_PASSWORD,user.getPassword());
        values.put(COLUMN_EMAIL,user.getMail());
        values.put(COLUMN_PHONE,user.getPhone());

        SQLiteDatabase db=getWritableDatabase();
        long x = db.insert(TABLE_NAME,null,values);
        Log.d("testss : ",String.valueOf(x));
        db.close();
    }

    //select user
    public User selectUser(String email){

        User user=new User();
        SQLiteDatabase db=getWritableDatabase();
        String query="SELECT * FROM "+ TABLE_NAME +" WHERE "+COLUMN_EMAIL+" = '"+email+"' ;";
        Cursor c=db.rawQuery(query,null);
        if(c!=null && c.getCount()>0){
            c.moveToFirst();
            while(!c.isAfterLast()){
                user.set_name( c.getString(c.getColumnIndex(COLUMN_USERNAME)) );
                user.set_password( c.getString(c.getColumnIndex(COLUMN_PASSWORD)) );
                user.set_mail( c.getString(c.getColumnIndex(COLUMN_EMAIL)) );
                user.set_phone( c.getString(c.getColumnIndex(COLUMN_PHONE)) );
                c.moveToNext();
            }
        }
        c.close();
        db.close();
        return user;
    }

    public Boolean checkAccess(User user){

        SQLiteDatabase db=getWritableDatabase();
        String query="SELECT * FROM "+ TABLE_NAME +" WHERE "
                +COLUMN_USERNAME + " = '" + user.getName() + "' and "
                +COLUMN_PASSWORD + " = '" + user.getPassword() + "' ;";
        Cursor c=db.rawQuery(query,null);
        if(c!=null&&c.getCount()>0){
            c.close();
            db.close();
            return true;
        }
        c.close();
        db.close();
        return false;
    }
}
