package com.group.alyasser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by OmarAli on 27/01/2016.
 */
public class TextValidator {

    public static Boolean IsValid(String text) {
        Pattern p = Pattern.compile("([0-9])");
        Matcher m = p.matcher(text);

        if (m.find()) {
            return false;
        }
        return true;
    }
}
