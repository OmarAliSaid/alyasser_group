package com.group.alyasser;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.group.alyasser.database.model.User;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

public class Signup extends AppCompatActivity implements Button.OnClickListener,Validator.ValidationListener{

    /*  annotations are used to create constrain on what type of data should EditText have
     *  that is how saripaar library works
     *  */

    @NotEmpty(message = "من فضلك ادخل اسمك")
    EditText et_userName;
    @NotEmpty(message = "من فضلك ادخل رقمك")
    EditText et_phone;
    @Password(min = 6,message = "ادخل كلمة سر صحيحه")
    EditText et_pass;
    @ConfirmPassword(message = "تأكد من تطابق كلمة السر")
    EditText et_repeatedPass;
    @Email(message = "بريد الكترونى غير صحيح")
    EditText et_mail;

    Button btn_register;
    Validator validator;

    public void Initialize(){
        et_userName=(EditText)findViewById(R.id.et_user);
        et_pass=(EditText)findViewById(R.id.et_pass);
        et_repeatedPass=(EditText)findViewById(R.id.et_repeatedPass);
        et_mail=(EditText)findViewById(R.id.et_email);
        et_phone=(EditText)findViewById(R.id.et_phone);
        btn_register=(Button)findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Initialize();
    }

    @Override
    public void onClick(View view) {
            validator.validate();
    }

    @Override
    public void onValidationSucceeded() {

        //check if user name contain numbers
        if(TextValidator.IsValid(et_userName.getText().toString())){
            User user=BuildUser();
            /* check if user already in database
            * check is done by user email since it is unique
            * if (user.mail) is Null then insert him cause it's a prove that he is not on the database
            */
            if(user.getData(this).getMail()==null){
                user.Insert(this);
                Toast.makeText(this,"تم التسجيل بنجاح",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this,"المستخدم موجود بالفعل",Toast.LENGTH_LONG).show();
            }
        }else{
            et_userName.setError("الاسم غير صحيح");
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    public User BuildUser(){
        String name=et_userName.getText().toString();
        String pass = et_pass.getText().toString();
        String mail=et_mail.getText().toString();
        String phone=et_phone.getText().toString();
        return new User(name,pass,mail,phone);
    }
}
