package com.group.alyasser;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.group.alyasser.database.model.User;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

public class Login extends AppCompatActivity implements Button.OnClickListener,Validator.ValidationListener {

    Button login,signUp;
    @NotEmpty(message = "من فضلك ادخل اسمك") // not empty constrain with error message
    EditText et_userName;
    @NotEmpty(message = "من فضلك أدخل كلمة السر")  // not empty constrain with error message
    EditText et_password;

    Validator validator;

    /**
     * linking screen views and initialize validator
     * validator used to validate username and password
    */

    public void initialize(){

        login=(Button)findViewById(R.id.btn_login);
        signUp=(Button)findViewById(R.id.btn_signup);
        et_userName=(EditText)findViewById(R.id.et_username);
        et_password=(EditText)findViewById(R.id.et_password);
        login.setOnClickListener(this);
        signUp.setOnClickListener(this);

        validator = new Validator(this);
        //set listener to listen to the two EditText (user name , password)
        validator.setValidationListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialize();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.btn_signup:
                intent=new Intent(this,Signup.class);
                startActivity(intent);
                break;
            case R.id.btn_login:
                //start validating the views
                validator.validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {

        String name=et_userName.getText().toString();
        String pass = et_password.getText().toString();

        //check if user name contain numbers
        if(TextValidator.IsValid(et_userName.getText().toString())){
            //create new user with entered data from EditText (userName , password)
            User user=new User(name,pass);

            //check if user have access
            if(user.IsHaveAccess(this)==true){
                Toast.makeText(this,"تسجيل صحيح",Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(this,"انت غير مسجل",Toast.LENGTH_LONG).show();
            }
        }else{
            et_userName.setError("الاسم غير صحيح");
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        //for each view error print a message of that error
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
